#!/usr/bin/env bash

python3 -m unittest discover ./tests || exit 1;

exit 0;
