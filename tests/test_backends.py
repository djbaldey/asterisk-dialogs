#
# Copyright (c) 2021, Grigoriy Kramarenko
# All rights reserved.
# This file is distributed under the same license as the current project.
#
import unittest
from io import BytesIO


class TestBase(unittest.TestCase):

    def test_listener(self):
        from astersay.backends.base import Listener

        data = b'1234567890'
        listener = Listener(stream=BytesIO(data))
        self.assertIs(listener._continue, True)
        iterator = listener.listen()
        chunk = next(iterator)
        self.assertEqual(chunk, data)
        self.assertEqual(listener.total_size, 10)
        self.assertIs(listener._continue, True)
        listener.stop()
        self.assertIs(listener._continue, False)


class TestTinkoff(unittest.TestCase):

    def test_imports(self):
        from astersay.backends.tinkoff import TinkoffRecognizer  # NOQA
        from astersay.backends.tinkoff import TinkoffSynthesizer  # NOQA


class TestVosk(unittest.TestCase):

    def test_imports(self):
        from astersay.backends.vosk import VoskRecognizer  # NOQA


class TestYandex(unittest.TestCase):

    def test_imports(self):
        from astersay.backends.yandex import YandexRecognizer  # NOQA
        from astersay.backends.yandex import YandexSynthesizer  # NOQA
