# Для работы виртуального сервера DummyAsterisk необходим PyAudio.
# Для его установки нужно предварительно в системе иметь пакеты:
#   sudo apt install gcc portaudio19-dev
#   sudo yum install gcc portaudio-devel
pyaudio>=0.2.11,<1.0a

# Vosk - это тулкит и сервер для голосового распознавания. Для соединения с
# сервером необходим клиентский пакет для Web-сокетов.
websocket-client>=0.50.0,<1.0a
